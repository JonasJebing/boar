# boar

Flexible clone on write smart pointers with **B**orrowed, **O**wned, **A**rc and **R**c variants.
Like std's Cow without the ToOwned requirement and additional Rc or Arc variants.

Have a look at the [documentation](https://docs.rs/boar/) for more information.

## Safety

This crate has no unsafe code and uses `#![forbid(unsafe_code)]`.

## License

Licensed under either of [Apache License, Version 2.0](LICENSE-APACHE)
or [MIT license](LICENSE-MIT)
at your option.

### Contribution

Unless you explicitly state otherwise,
any contribution intentionally submitted for inclusion in this crate by you,
as defined in the Apache-2.0 license,
shall be dual licensed as above,
without any additional terms or conditions.
