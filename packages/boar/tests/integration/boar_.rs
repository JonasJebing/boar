use boar::{Boar, BoarTo};
use std::collections::HashMap;

type BoarArray<'b, T, const N: usize> = Boar<'b, [T], [T; N]>;
type Str<'b> = BoarTo<'b, str>;

#[test]
fn cmp() {
    assert_eq!(Str::Borrowed("a"), "a");
    assert!(Str::Borrowed("a") >= "a");
}

#[test]
fn array_from_array() {
    let array = [2_i32; 8];
    assert_eq!(BoarArray::Owned(array), BoarArray::from(array));
}

#[test]
fn map_from_array() {
    let netherlands = "The Netherlands";
    let array = [
        ("Amsterdam", netherlands),
        ("The Hague", netherlands),
        ("Berlin", "Germany"),
        ("Copenhagen", "Denmark"),
    ];
    let boar = Boar::<HashMap<_, _>>::from(array);
    assert_eq!(boar, Boar::Owned(HashMap::from(array)));
}
