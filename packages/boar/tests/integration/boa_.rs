use boar::{Boa, BoaTo};
use std::collections::HashMap;

type BoaArray<'b, T, const N: usize> = Boa<'b, [T], [T; N]>;
type Str<'b> = BoaTo<'b, str>;

#[test]
fn cmp() {
    assert_eq!(Str::Borrowed("a"), "a");
    assert!(Str::Borrowed("a") >= "a");
}

#[test]
fn array_from_array() {
    let array = [2_i32; 8];
    assert_eq!(BoaArray::Owned(array), BoaArray::from(array));
}

#[test]
fn map_from_array() {
    let netherlands = "The Netherlands";
    let array = [
        ("Amsterdam", netherlands),
        ("The Hague", netherlands),
        ("Berlin", "Germany"),
        ("Copenhagen", "Denmark"),
    ];
    let boa = Boa::<HashMap<_, _>>::from(array);
    assert_eq!(boa, Boa::Owned(HashMap::from(array)));
}
