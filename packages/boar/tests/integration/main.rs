mod boa_;
mod boar_;

#[test]
fn size_of_boa_str() {
    use boar::BoasStr;
    use std::borrow::Cow;
    use std::mem::size_of;

    dbg!(size_of::<&str>());
    dbg!(size_of::<String>());
    dbg!(size_of::<BoasStr>());
    dbg!(size_of::<Cow<str>>());
    assert_eq!(size_of::<Cow<str>>(), size_of::<BoasStr>());
}
