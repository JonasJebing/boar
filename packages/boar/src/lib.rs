/*!
This crate provides flexible clone on write smart pointers
with Borrowed, Owned, Arc, Rc and Static variants.
They are like std's [`Cow`][std::borrow::Cow]
but with additonal [`Arc`][std::sync::Arc], [`Rc`][std::rc::Rc]
and `Static` variants.
Unlike std's [`Cow`][std::borrow::Cow],
they can also be used with types
that don't implement [`Clone`] or [`ToOwned`].

The `boar` smart pointers allow you to:

- share data efficiently
- decide ownership at runtime
- avoid cloning the data until it becomes necessary

# Examples

```rust
# fn main() -> std::io::Result<()> {
use std::sync::Arc;
use boar::BoasStr;

// No need to allocate memory for a &'static str
let title = BoasStr::Static("Dune");

// Also no need to allocate memory for a &str, that's valid for the required scope
let title = read_file("title.txt")?;
let title = BoasStr::Borrowed(&title);

// The author's name shouldn't be too long.
// So we can just store it in a normal String.
let author = BoasStr::Owned(read_file("dune/author.txt")?);

// A whole book is really long.
// And we want to share this string with multiple threads.
// So it would be efficient to store in an Arc,
// since cloning the Arc does not clone the String data inside of it.
let book_text = BoasStr::Arc(Arc::new(read_file("dune/book.txt")?));

// We can store a &str, String and Arc<String> inside the same Vec
// because we are using the BoasStr enum.
let book_infos: Vec<BoasStr> = Vec::from([title, author, book_text]);

fn read_file(path: &str) -> std::io::Result<String> {
    // ...
# Ok(String::new())
}
# Ok(()) }
```
*/
#![forbid(unsafe_code)]

mod boa;
mod boar;
mod boars;
mod boas;

pub use self::boa::{Boa, BoaBox, BoaTo, BoaVec};
pub use self::boar::{Boar, BoarBox, BoarTo, BoarVec};
pub use self::boars::{Boars, BoarsBox, BoarsStr, BoarsTo, BoarsVec};
pub use self::boas::{Boas, BoasBox, BoasStr, BoasTo, BoasVec};
