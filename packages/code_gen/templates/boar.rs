use std::{
    //__FromNonStaticStart__
    borrow::Cow,
    //__FromNonStaticEnd__
    borrow::{Borrow, BorrowMut},
    cmp::Ordering,
    collections::HashMap,
    //__StaticStart__
    ffi::{CStr, CString, OsStr, OsString},
    //__StaticEnd__
    fmt,
    hash::Hash,
    iter::FromIterator,
    ops::{Add, AddAssign, Deref},
    //__StaticStart__
    path::{Path, PathBuf},
    //__StaticEnd__
    //__RcStart__
    rc::Rc,
    //__RcEnd__
    //__ArcStart__
    sync::Arc,
    //__ArcEnd__
};

/// __doc_variants__ smart pointer for types implementing the [`ToOwned`] (**T**.**O**.) trait.
pub type __Boar__To<'b, B> = __Boar__<'b, B, <B as ToOwned>::Owned>;

/// __doc_variants__ heap allocating smart pointer.
pub type __Boar__Box<'b, T> = __Boar__<'b, T, Box<T>, T>;

/// __doc_variants__ [`Vec`] smart pointer.
pub type __Boar__Vec<'b, T> = __Boar__To<'b, [T]>;

//__StaticStart__
/// __doc_variants__ [`str`] smart pointer.
pub type __Boar__Str<'b> = __Boar__To<'b, str>;
//__StaticEnd__

/// __doc_variants__ smart pointer.
#[non_exhaustive]
pub enum __Boar__<'b, B, O = B, S = O>
where
    B: ?Sized,
    //__StaticStart__
    B: 'static,
    //__StaticEnd__
    S: ?Sized,
{
    Borrowed(&'b B),

    Owned(O),

    //__ArcStart__
    // unstable reason:
    // Variant may not be needed.
    // Arc::clone may be good enough.
    // Upgrading and downgrading lifetimes may be used rarely.
    #[cfg(feature = "unstable")]
    BorrowedArc(&'b Arc<S>),
    Arc(Arc<S>),
    //__ArcEnd__
    //__RcStart__
    // unstable reason:
    // Variant may not be needed.
    // Rc::clone may be good enough.
    // Upgrading and downgrading lifetimes may be used rarely.
    #[cfg(feature = "unstable")]
    BorrowedRc(&'b Rc<S>),
    Rc(Rc<S>),
    //__RcEnd__
    //__StaticStart__
    Static(&'static B),
    //__StaticEnd__
}

impl<'b, B: ?Sized, O, S: ?Sized> __Boar__<'b, B, O, S> {
    /// Extracts the owned data.
    ///
    /// Clones the data if it is not already owned.
    ///
    /// # Examples
    ///
    /// Calling `into_owned` on a `__Boar__::Borrowed` clones the inner data.
    /// ```
    /// use boar::__Boar__To;
    /// use std::collections::HashMap;
    ///
    /// let map = HashMap::from([(1, 4), (2, 8)]);
    /// let __boar__ = __Boar__To::Borrowed(&map);
    /// let owned: HashMap<_, _> = __boar__.into_owned();
    /// assert_eq!(owned, map);
    /// ```
    ///
    /// Calling `into_owned` on a `__Boar__::Owned` doesn't clone anything.
    /// ```
    /// use boar::__Boar__To;
    /// use std::collections::HashMap;
    ///
    /// let __boar__ = __Boar__To::<HashMap<_, _>>::Owned(HashMap::from([(1, 4)]));
    /// let owned: HashMap<_, _> = __boar__.into_owned();
    /// assert_eq!(owned, HashMap::from([(1, 4)]));
    /// ```
    #[inline]
    pub fn into_owned(self) -> O
    where
        B: ToOwned<Owned = O>,
        S: Clone + Into<O>,
    {
        match self {
            Self::Borrowed(x) => x.to_owned(),
            //__StaticStart__
            Self::Static(x) => x.to_owned(),
            //__StaticEnd__
            Self::Owned(x) => x,
            //__ArcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedArc(x) => x.deref().clone().into(),
            Self::Arc(x) => Arc::try_unwrap(x)
                .unwrap_or_else(|x| x.deref().clone())
                .into(),
            //__ArcEnd__
            //__RcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedRc(x) => x.deref().clone().into(),
            Self::Rc(x) => Rc::try_unwrap(x)
                .unwrap_or_else(|x| x.deref().clone())
                .into(),
            //__RcEnd__
        }
    }

    /// Acquires a mutable reference to the owned form of the data.
    ///
    /// Clones the data if it is not already owned.
    ///
    /// # Examples
    ///
    /// Calling `to_mut` on a `__Boar__::Borrowed` clones the inner data.
    /// ```
    /// use boar::__Boar__To;
    /// use std::collections::HashMap;
    ///
    /// let map = HashMap::from([(1, 4), (2, 8)]);
    /// let mut __boar__ = __Boar__To::Borrowed(&map);
    /// __boar__.to_mut().remove(&1);
    /// assert_eq!(__boar__, HashMap::from([(2, 8)]));
    /// ```
    ///
    /// Calling `to_mut` on a `__Boar__::Owned` doesn't clone anything.
    /// ```
    /// use boar::__Boar__To;
    /// use std::collections::HashMap;
    ///
    /// let mut __boar__ = __Boar__To::Owned(HashMap::from([(1, 4), (2, 8)]));
    /// __boar__.to_mut().remove(&1);
    /// assert_eq!(__boar__, HashMap::from([(2, 8)]));
    /// ```
    #[inline]
    pub fn to_mut(&mut self) -> &mut O
    where
        B: ToOwned<Owned = O>,
        S: Clone + BorrowMut<O>,
    {
        match self {
            Self::Borrowed(x) => {
                *self = Self::Owned(x.to_owned());
                self.to_mut()
            }
            //__StaticStart__
            Self::Static(x) => {
                *self = Self::Owned(x.to_owned());
                self.to_mut()
            }
            //__StaticEnd__
            Self::Owned(x) => x,
            //__ArcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedArc(x) => {
                // avoid changing the reference count by immediately cloning S
                *self = Self::Arc(S::clone(x).into());
                self.to_mut()
            }
            Self::Arc(x) => Arc::make_mut(x).borrow_mut(),
            //__ArcEnd__
            //__RcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedRc(x) => {
                // avoid changing the reference count by immediately cloning S
                *self = Self::Rc(S::clone(x).into());
                self.to_mut()
            }
            Self::Rc(x) => Rc::make_mut(x).borrow_mut(),
            //__RcEnd__
        }
    }

    /**
    Convert borrowed variants so that the new `__Boar__` has a `'static` lifetime.

    # Examples

    ```rust
    use boar::{__Boar__, __Boar__To};
    use std::collections::HashMap;

    let map = HashMap::from([(1, 3), (2, 6)]);
    let b = __Boar__To::Borrowed(&map);
    let s = b.into_static();
    assert_eq!(s, map);
    assert_matches::assert_matches!(s, __Boar__::Owned(_));
    ```
    */
    #[inline]
    pub fn into_static(self) -> __Boar__<'static, B, O, S>
    where
        B: ToOwned<Owned = O>,
    {
        match self {
            Self::Borrowed(x) => __Boar__::Owned(x.to_owned()),
            Self::Owned(x) => __Boar__::Owned(x),
            //__ArcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedArc(x) => __Boar__::Arc(x.clone()),
            Self::Arc(x) => __Boar__::Arc(x),
            //__ArcEnd__
            //__RcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedRc(x) => __Boar__::Rc(x.clone()),
            Self::Rc(x) => __Boar__::Rc(x),
            //__RcEnd__
            //__StaticStart__
            Self::Static(x) => __Boar__::Static(x),
            //__StaticEnd__
        }
    }

    /**
    Convert to a new `__Boar__` that borrows from `self`.

    # Compatibility

    The variants returned by this function are an implementation detail
    and may change with any new minor version.
    So for example your code should not rely on this function
    returning a [`__Boar__::Borrowed`] variant
    when it is called on a [`__Boar__::Owned`] variant.

    # Examples

    ```rust
    use boar::{__Boar__, __Boar__To};
    use std::collections::HashMap;

    let map = HashMap::from([(1, 3), (2, 6)]);
    let o: __Boar__<_> = __Boar__To::Owned(map.clone());
    let b: __Boar__<_> = o.to_borrowed(); // does not clone the map
    assert_eq!(o, map);
    assert_eq!(b, map);
    ```
    */
    #[inline]
    pub fn to_borrowed(&self) -> __Boar__<'_, B, O, S>
    where
        O: Borrow<B>,
    {
        match self {
            Self::Borrowed(x) => __Boar__::Borrowed(x),
            Self::Owned(x) => __Boar__::Borrowed(x.borrow()),
            //__ArcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedArc(x) => __Boar__::BorrowedArc(x),
            #[cfg(feature = "unstable")]
            Self::Arc(x) => __Boar__::BorrowedArc(x),
            #[cfg(not(feature = "unstable"))]
            Self::Arc(x) => __Boar__::Arc(x.clone()),
            //__ArcEnd__
            //__RcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedRc(x) => __Boar__::BorrowedRc(x),
            #[cfg(feature = "unstable")]
            Self::Rc(x) => __Boar__::BorrowedRc(x),
            #[cfg(not(feature = "unstable"))]
            Self::Rc(x) => __Boar__::Rc(x.clone()),
            //__RcEnd__
            //__StaticStart__
            Self::Static(x) => __Boar__::Static(x),
            //__StaticEnd__
        }
    }

    /**
    Convert [`__Boar__::Owned`] into [`__Boar__::__PreferredRc__`].

    # Examples

    ```rust
    use boar::{__Boar__, __Boar__To};
    use std::collections::HashMap;

    let map = HashMap::from([(1, 3)]);
    let o = __Boar__To::Owned(map.clone());
    let s = o.into_shared();
    assert_eq!(s, map);
    assert_matches::assert_matches!(s, __Boar__::__PreferredRc__(_));
    ```
    */
    #[inline]
    pub fn into_shared(self) -> Self
    where
        O: Into<__PreferredRc__<S>>,
    {
        match self {
            Self::Owned(x) => Self::__PreferredRc__(x.into()),
            _ => self,
        }
    }

    /// This is **unstable** and only available with the `unstable` feature.
    #[cfg(feature = "unstable")] // reason: unclear if actually needed; `make_shared` and `clone` can be used instead.
    #[inline]
    pub fn clone_shared(&mut self) -> Self
    where
        O: Default + Clone + Into<__PreferredRc__<S>>,
    {
        self.internal_make_shared();
        self.clone()
    }

    /**
    Change [`__Boar__::Owned`] into [`__Boar__::__PreferredRc__`].

    # Examples

    ```rust
    use boar::{__Boar__, __Boar__To};
    use std::collections::HashMap;

    let map = HashMap::from([(1, 3), (2, 6)]);
    let mut __boar__ = __Boar__To::Owned(map.clone());
    __boar__.make_shared();
    assert_eq!(__boar__, map);
    assert_matches::assert_matches!(__boar__, __Boar__::__PreferredRc__(_));
    ```
    */
    #[inline]
    pub fn make_shared(&mut self)
    where
        O: Default + Into<__PreferredRc__<S>>,
    {
        self.internal_make_shared()
    }

    #[inline]
    fn internal_make_shared(&mut self)
    where
        O: Default + Into<__PreferredRc__<S>>,
    {
        if let Self::Owned(x) = self {
            let x = std::mem::take(x);
            *self = Self::__PreferredRc__(x.into());
        }
    }

    #[inline]
    fn internal_borrow(&self) -> &B
    where
        O: Borrow<B>,
        S: Borrow<B>,
    {
        match self {
            Self::Borrowed(x) => x,
            //__StaticStart__
            Self::Static(x) => x,
            //__StaticEnd__
            Self::Owned(x) => x.borrow(),
            //__ArcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedArc(x) => x.deref().deref().borrow(),
            Self::Arc(x) => x.deref().borrow(),
            //__ArcEnd__
            //__RcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedRc(x) => x.deref().deref().borrow(),
            Self::Rc(x) => x.deref().borrow(),
            //__RcEnd__
        }
    }
}

//__StaticStart__
impl<B: ?Sized, O, S: ?Sized> __Boar__<'static, B, O, S> {
    /// Upgrades [`__Boar__::Borrowed`] to [`__Boar__::Static`]
    ///
    /// Can only be called, when `Self` already has a `'static` lifetime.
    ///
    /// # Examples
    /// ```rust
    /// use boar::{__Boar__, __Boar__Str};
    ///
    /// let string = "Hello";
    /// let mut __boar__ = __Boar__Str::Borrowed(string);
    /// __boar__.ensure_static();
    /// assert_matches::assert_matches!(__boar__, __Boar__::Static(_));
    /// ```
    /// This example demonstrates the usefulness for a parse function:
    /// ```rust
    /// use boar::{__Boar__, __Boar__Str};
    ///
    /// struct Name<'s>(__Boar__Str<'s>);
    ///
    /// impl<'s> Name<'s> {
    ///     fn parse_str(string: &'s str) -> Self {
    ///         // Let's pretend this function actually does some complex parsing.
    ///         // We can't use `__Boar__Str::Static` because `string` has a generic lifetime.
    ///         Self(__Boar__Str::Borrowed(string))
    ///     }
    /// }
    ///
    /// let mut name = Name::parse_str("Alice");
    /// name.0.ensure_static();
    /// assert_matches::assert_matches!(name.0, __Boar__::Static(_));
    /// ```
    pub fn ensure_static(&mut self) {
        if let Self::Borrowed(x) = *self {
            *self = Self::Static(x);
        }
    }

    /// Upgrades [`__Boar__::Borrowed`] into [`__Boar__::Static`]
    ///
    /// Can only be called, when `Self` already has a `'static` lifetime.
    ///
    /// # Examples
    /// ```rust
    /// use boar::{__Boar__, __Boar__Str};
    ///
    /// let string = "Hello";
    /// let __boar__ = __Boar__Str::Borrowed(string);
    /// assert_matches::assert_matches!(
    ///     __boar__.into_ensured_static(),
    ///     __Boar__::Static(_)
    /// );
    /// ```
    /// This example demonstrates the usefulness for a parse function:
    /// ```rust
    /// use boar::{__Boar__, __Boar__Str};
    ///
    /// struct Name<'s>(__Boar__Str<'s>);
    ///
    /// impl<'s> Name<'s> {
    ///     fn parse_str(string: &'s str) -> Self {
    ///         // Let's pretend this function actually does some complex parsing.
    ///         // We can't use `__Boar__Str::Static` because `string` has a generic lifetime.
    ///         Self(__Boar__Str::Borrowed(string))
    ///     }
    /// }
    ///
    /// let name = Name::parse_str("Alice");
    /// let static_name = name.0.into_ensured_static();
    /// assert_matches::assert_matches!(static_name, __Boar__::Static(_));
    /// ```
    pub fn into_ensured_static(mut self) -> Self {
        self.ensure_static();
        self
    }
}
//__StaticEnd__

impl<'b, B: ?Sized, O, S: ?Sized, Rhs> Add<Rhs> for __Boar__<'b, B, O, S>
where
    O: Add<Rhs>,
    B: ToOwned<Owned = O>,
    S: Clone + Into<O>,
{
    type Output = <O as Add<Rhs>>::Output;

    #[inline]
    fn add(self, rhs: Rhs) -> <O as Add<Rhs>>::Output {
        self.into_owned() + rhs
    }
}

impl<'b, B: ?Sized, O, S: ?Sized, Rhs> AddAssign<Rhs> for __Boar__<'b, B, O, S>
where
    O: AddAssign<Rhs>,
    B: ToOwned<Owned = O>,
    S: Clone + BorrowMut<O>,
{
    #[inline]
    fn add_assign(&mut self, rhs: Rhs) {
        *self.to_mut() += rhs;
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> AsRef<B> for __Boar__<'b, B, O, S>
where
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn as_ref(&self) -> &B {
        self.internal_borrow()
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Borrow<B> for __Boar__<'b, B, O, S>
where
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn borrow(&self) -> &B {
        self.internal_borrow()
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Deref for __Boar__<'b, B, O, S>
where
    O: Borrow<B>,
    S: Borrow<B>,
{
    type Target = B;

    #[inline]
    fn deref(&self) -> &Self::Target {
        self.internal_borrow()
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Clone for __Boar__<'b, B, O, S>
where
    O: Clone,
{
    #[inline]
    fn clone(&self) -> Self {
        match self {
            Self::Borrowed(x) => Self::Borrowed(x),
            //__StaticStart__
            Self::Static(x) => Self::Static(x),
            //__StaticEnd__
            Self::Owned(x) => Self::Owned(x.clone()),
            //__ArcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedArc(x) => Self::BorrowedArc(x),
            Self::Arc(x) => Self::Arc(x.clone()),
            //__ArcEnd__
            //__RcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedRc(x) => Self::BorrowedRc(x),
            Self::Rc(x) => Self::Rc(x.clone()),
            //__RcEnd__
        }
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Default for __Boar__<'b, B, O, S>
where
    O: Default,
{
    /// Creates an owned __Boar__ with the default value for the contained owned type.
    #[inline]
    fn default() -> Self {
        Self::Owned(O::default())
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> fmt::Debug for __Boar__<'b, B, O, S>
where
    B: fmt::Debug,
    O: fmt::Debug,
    S: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            __Boar__::Borrowed(x) => f.debug_tuple("__Boar__::Borrowed").field(x).finish(),
            __Boar__::Owned(x) => f.debug_tuple("__Boar__::Owned").field(x).finish(),
            //__ArcStart__
            #[cfg(feature = "unstable")]
            __Boar__::BorrowedArc(x) => f.debug_tuple("__Boar__::BorrowedArc").field(x).finish(),
            __Boar__::Arc(x) => f.debug_tuple("__Boar__::Arc").field(x).finish(),
            //__ArcEnd__
            //__RcStart__
            #[cfg(feature = "unstable")]
            __Boar__::BorrowedRc(x) => f.debug_tuple("__Boar__::BorrowedRc").field(x).finish(),
            __Boar__::Rc(x) => f.debug_tuple("__Boar__::Rc").field(x).finish(),
            //__RcEnd__
            //__StaticStart__
            __Boar__::Static(x) => f.debug_tuple("__Boar__::Static").field(x).finish(),
            //__StaticEnd__
        }
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> fmt::Display for __Boar__<'b, B, O, S>
where
    B: fmt::Display,
    O: fmt::Display,
    S: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Borrowed(x) => fmt::Display::fmt(x, f),
            Self::Owned(x) => fmt::Display::fmt(x, f),
            //__ArcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedArc(x) => fmt::Display::fmt(x, f),
            Self::Arc(x) => fmt::Display::fmt(x, f),
            //__ArcEnd__
            //__RcStart__
            #[cfg(feature = "unstable")]
            Self::BorrowedRc(x) => fmt::Display::fmt(x, f),
            Self::Rc(x) => fmt::Display::fmt(x, f),
            //__RcEnd__
            //__StaticStart__
            Self::Static(x) => fmt::Display::fmt(x, f),
            //__StaticEnd__
        }
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Hash for __Boar__<'b, B, O, S>
where
    B: Hash,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.internal_borrow().hash(state)
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialEq<&B> for __Boar__<'b, B, O, S>
where
    B: PartialEq,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn eq(&self, other: &&B) -> bool {
        self.internal_borrow() == *other
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialEq<B> for __Boar__<'b, B, O, S>
where
    B: PartialEq,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn eq(&self, other: &B) -> bool {
        self.internal_borrow() == other
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialEq for __Boar__<'b, B, O, S>
where
    B: PartialEq,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn eq(&self, other: &Self) -> bool {
        self.internal_borrow() == other.internal_borrow()
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Eq for __Boar__<'b, B, O, S>
where
    B: Eq,
    O: Borrow<B>,
    S: Borrow<B>,
{
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialOrd<&B> for __Boar__<'b, B, O, S>
where
    B: PartialOrd,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn partial_cmp(&self, other: &&B) -> Option<Ordering> {
        self.internal_borrow().partial_cmp(*other)
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialOrd<B> for __Boar__<'b, B, O, S>
where
    B: PartialOrd,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn partial_cmp(&self, other: &B) -> Option<Ordering> {
        self.internal_borrow().partial_cmp(other)
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> PartialOrd for __Boar__<'b, B, O, S>
where
    B: PartialOrd,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.internal_borrow().partial_cmp(other.internal_borrow())
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> Ord for __Boar__<'b, B, O, S>
where
    B: Ord,
    O: Borrow<B>,
    S: Borrow<B>,
{
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.internal_borrow().cmp(other.internal_borrow())
    }
}

impl<'b, A, B: ?Sized, O, S: ?Sized> FromIterator<A> for __Boar__<'b, B, O, S>
where
    O: FromIterator<A>,
{
    #[inline]
    fn from_iter<T: IntoIterator<Item = A>>(iter: T) -> Self {
        Self::Owned(iter.into_iter().collect())
    }
}

impl<'b, B: ToOwned + ?Sized + 'b> From<Cow<'b, B>> for __Boar__<'b, B, B::Owned, B::Owned> {
    #[inline]
    fn from(p: Cow<'b, B>) -> Self {
        match p {
            Cow::Borrowed(x) => Self::Borrowed(x),
            Cow::Owned(x) => Self::Owned(x),
        }
    }
}

impl<'b, B: ?Sized, O, S: ?Sized> From<&'b B> for __Boar__<'b, B, O, S> {
    #[inline]
    fn from(x: &'b B) -> Self {
        Self::Borrowed(x)
    }
}

//__ArcStart__
impl<'b, B: ?Sized, O, S: ?Sized> From<Arc<S>> for __Boar__<'b, B, O, S> {
    #[inline]
    fn from(x: Arc<S>) -> Self {
        Self::Arc(x)
    }
}
//__ArcEnd__

//__RcStart__
impl<'b, B: ?Sized, O, S: ?Sized> From<Rc<S>> for __Boar__<'b, B, O, S> {
    #[inline]
    fn from(x: Rc<S>) -> Self {
        Self::Rc(x)
    }
}
//__RcEnd__

impl<'b, T> From<Vec<T>> for __Boar__<'b, [T], Vec<T>> {
    #[inline]
    fn from(x: Vec<T>) -> Self {
        Self::Owned(x)
    }
}

impl<'b, T> From<__Boar__<'b, [T], Vec<T>>> for Vec<T>
where
    T: Clone,
    [T]: ToOwned<Owned = Vec<T>>,
    Vec<T>: Clone,
{
    #[inline]
    fn from(x: __Boar__<'b, [T], Vec<T>>) -> Self {
        x.into_owned()
    }
}

impl<'b, K, V, S> From<HashMap<K, V, S>> for __Boar__<'b, HashMap<K, V, S>> {
    #[inline]
    fn from(x: HashMap<K, V, S>) -> Self {
        Self::Owned(x)
    }
}

//__StaticStart__
impl<'b> From<String> for __Boar__<'b, str, String> {
    #[inline]
    fn from(x: String) -> Self {
        Self::Owned(x)
    }
}

impl<'b> From<__Boar__<'b, str, String>> for String {
    #[inline]
    fn from(x: __Boar__<'b, str, String>) -> Self {
        x.into_owned()
    }
}

impl<'b> From<CString> for __Boar__<'b, CStr, CString> {
    #[inline]
    fn from(x: CString) -> Self {
        Self::Owned(x)
    }
}

impl<'b> From<__Boar__<'b, CStr, CString>> for CString {
    #[inline]
    fn from(x: __Boar__<'b, CStr, CString>) -> Self {
        x.into_owned()
    }
}

impl<'b> From<OsString> for __Boar__<'b, OsStr, OsString> {
    #[inline]
    fn from(x: OsString) -> Self {
        Self::Owned(x)
    }
}

impl<'b> From<__Boar__<'b, OsStr, OsString>> for OsString {
    #[inline]
    fn from(x: __Boar__<'b, OsStr, OsString>) -> Self {
        x.into_owned()
    }
}

impl<'b> From<PathBuf> for __Boar__<'b, Path, PathBuf> {
    #[inline]
    fn from(x: PathBuf) -> Self {
        Self::Owned(x)
    }
}

impl<'b> From<__Boar__<'b, Path, PathBuf>> for PathBuf {
    #[inline]
    fn from(x: __Boar__<'b, Path, PathBuf>) -> Self {
        x.into_owned()
    }
}
//__StaticEnd__

impl<'b, B, O, S, T, const N: usize> From<[T; N]> for __Boar__<'b, B, O, S>
where
    B: ?Sized,
    O: From<[T; N]>,
    S: ?Sized,
{
    #[inline]
    fn from(x: [T; N]) -> Self {
        Self::Owned(x.into())
    }
}
