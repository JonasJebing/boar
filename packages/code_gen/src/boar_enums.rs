use crate::GENERATED_FILE_COMMENT;
use std::{fs, iter, ops::RangeInclusive};

const ENUM_TEMPLATE: &str = "packages/code_gen/templates/boar.rs";
const _ARC_RANGE: TagRange = "//__ArcStart__"..="//__ArcEnd__";
const RC_RANGE: TagRange = "//__RcStart__"..="//__RcEnd__";
const STATIC_RANGE: TagRange = "//__StaticStart__"..="//__StaticEnd__";

type StaticStr = &'static str;
type TagRange = RangeInclusive<StaticStr>;

pub fn generate() {
    let template = fs::read_to_string(ENUM_TEMPLATE).unwrap();
    generate_enum(
        &template,
        &Settings {
            target: "packages/boar/src/boa.rs",
            type_name: "Boa",
            variable_name: "boa",
            doc_variants: "**B**orrowed, **O**wned or **A**rc",
            preferred_rc: "Arc",
            excludes: Vec::from([RC_RANGE, STATIC_RANGE]),
        },
    );
    generate_enum(
        &template,
        &Settings {
            target: "packages/boar/src/boar.rs",
            type_name: "Boar",
            variable_name: "boar",
            doc_variants: "**B**orrowed, **O**wned, **A**rc or **R**c",
            preferred_rc: "Rc",
            excludes: Vec::from([STATIC_RANGE]),
        },
    );
    generate_enum(
        &template,
        &Settings {
            target: "packages/boar/src/boas.rs",
            type_name: "Boas",
            variable_name: "boas",
            doc_variants: "**B**orrowed, **O**wned, **A**rc or **S**tatic",
            preferred_rc: "Arc",
            excludes: Vec::from([RC_RANGE]),
        },
    );
    generate_enum(
        &template,
        &Settings {
            target: "packages/boar/src/boars.rs",
            type_name: "Boars",
            variable_name: "boars",
            doc_variants: "**B**orrowed, **O**wned, **A**rc, **R**c or **S**tatic",
            preferred_rc: "Rc",
            excludes: Vec::from([]),
        },
    );
}

#[derive(Debug, Clone)]
struct Settings {
    type_name: StaticStr,
    variable_name: StaticStr,
    doc_variants: StaticStr,
    preferred_rc: StaticStr,
    excludes: Vec<TagRange>,
    target: StaticStr,
}

fn generate_enum(template: &str, options: &Settings) {
    let content = generate_enum_string(template, options);
    fs::write(options.target, content).unwrap();
}

fn generate_enum_string(template: &str, options: &Settings) -> String {
    let lines = iter::once(GENERATED_FILE_COMMENT).chain(template.lines());
    let filtered: String = lines
        .filter(is_included(&options.excludes))
        .filter(|l| !is_range_tag(l))
        .flat_map(|s| [s, "\n"]) // add line endings
        .collect();
    filtered
        .replace("__doc_variants__", options.doc_variants)
        .replace("__Boar__", options.type_name)
        .replace("__boar__", options.variable_name)
        .replace("__PreferredRc__", options.preferred_rc)
}

fn is_included(excludes: &[TagRange]) -> impl FnMut(&&str) -> bool + '_ {
    let mut current_exclude: Option<TagRange> = None;
    move |line| {
        let line = line.trim();
        match &current_exclude {
            None => match excludes.iter().find(|r| r.start() == &line) {
                Some(exclude) => {
                    current_exclude = Some(exclude.clone());
                    false
                }
                None => true,
            },
            Some(exclude) => {
                if exclude.end() == &line {
                    current_exclude = None;
                }
                false
            }
        }
    }
}

fn is_range_tag(line: &str) -> bool {
    let line = line.trim();
    line.starts_with("//__") && line.ends_with("__")
}
